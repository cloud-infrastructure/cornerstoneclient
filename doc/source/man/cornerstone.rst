============
cornerstone
============

--------------------------
Cornerstone Client Command
--------------------------
:Author: jose.castro.leon@cern.ch
:Date:   2015-10-07
:Version: 1.1.0
:Manual section: 1


SYNOPSIS
========
  cornerstone [-k] [-r] [--username <auth-user-name>]
                   [--password <auth-password>] [--url <wsdl-url>]
                   <subcommand> ...

DESCRIPTION
===========

cornerstone is the command line tool that interacts with the cornerstone API
service to create, modify and delete projects in Cornerstone.

USAGE
=====

    ``cornerstone [options] action [additional args]``

General cornerstone actions:
----------------------------

Invoking cornerstone by itself will give you some usage information.

Available commands:

  * ``help`` : Display help about this program or one of its subcommands
  * ``allow-operators`` : Enable/disable operators access on a project
  * ``allow-supporters`` : (REST only) Enable/disable supporters access on a project
  * ``allow-service`` : Enable/disable access to different services on a project
  * ``create-secgroups`` : Adds default security groups
  * ``domain-list`` : List all available domains
  * ``person-add`` : (REST only) Add new person
  * ``person-delete`` : (REST only) Delete person
  * ``person-get`` : (REST only) Get information about person
  * ``person-list`` : (REST only) List all persons
  * ``person-update`` : (REST only) Update person information
  * ``project-create`` : Create new project
  * ``project-delete`` : Delete project and associated resources
  * ``project-get`` : Display project details
  * ``project-get-info`` : Display project extra metadata
  * ``project-list`` : List all projects
  * ``project-members-add`` : Adds the members from the array on the role on a given project
  * ``project-members-del`` : Delete the members from the array on the role on a given project
  * ``project-members-get`` : List the members on a role on an specific project
  * ``project-members-set`` : Sets the members from the array on the role on a given project
  * ``project-properties-add`` : (REST only) Add new property to project
  * ``project-properties-delete`` : (REST only) Delete project property
  * ``project-properties-list`` : (REST only) List project properties
  * ``project-properties-update`` : (REST only) Update property to project
  * ``project-service-list`` : (REST only) Returns the list of services enabled for a project
  * ``project-update`` : Update project name, description, enabled status, owner
  * ``schema-get`` : (REST only) Get all model schemas

OPTIONS
=======

  -k, --kerberos              Enable kerberos
  -r, --restful               Using RESTful calls
  --username USER             Specify the user while using basic authentication
                              Defaults to env[USERNAME]
  --password PASSWD           Specify the password while using basic authentication
                              Defaults to env[PASSWORD]
  --url <wsdl-url>            Specify the wsdl url for the Cornerstone API server
                              Defaults to env[WSDL_URL]
  --cert <request_ca-bundle>  Defaults to None

FILES
=====

None
