=============
Release notes
=============

1.1.0 (Oct 07, 2015)
=====================
- [os-1593] Add REST API to cornerstone
- [os-1678] Add logging to cornerstoneclient
- [os-1795] Complete Services API
- [os-1796] Add REST Properties call

1.0.3 (Jun 05, 2015)
======================
- [os-1531] Add allow-service command

1.0.2 (Dic 01, 2014)
======================
- First CentOS release

1.0.1 (Jun 26, 2014)
======================
- Documentation updated
- Added new feature domain-list

0.9 (Mar 12, 2014)
======================
- CRUD on role assignment for projects
- Improved lifecycle management for projects
- remove unused functionality

0.5.0 (Feb 21, 2014)
======================
* Allow accounting and operators actions

0.4.0 (Oct 24, 2013)
======================
* Set enabled by default on project update

0.3.0 (May 17, 2013)
======================
* Requires python-httplib2
* Added ownership management

0.2.0 (March 15, 2013)
======================
* Minor exception fix

0.1.0 (March 07, 2013)
======================
* First version of the tool
