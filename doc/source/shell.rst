The :program:`cornerstone` shell utility
=========================================

.. program:: cornerstone
.. highlight:: bash


The :program:`cornerstone` shell utility interacts with Cornerstone API
from the command line. It supports the entirely of the Cornerstone API.

To communicate with the API, you will need to be authenticated - and the
:program:`cornerstone` provides multiple options for this.

There is two possibilities to do the authentication using kerberos or by
specifying the username and password

With basic authentication you can specify those values on the command line
with :option:`--username` and :option:`--password`, or set them in 
environment variables:

.. envvar:: USERNAME

    Your username.

.. envvar:: PASSWORD

    Your password.

With kerberos you only need to specify in the command line :option:`-k` or
:option:`--kerberos`

You also need to specify the api server wsdl location using the command line
with :option:`--wsdl-url` or in an environment variable

.. envvar:: WSDL_URL

    Cornerstone API server WSDL location

The command line options will override any environment variables set.

Also, if you want to use RESTfull API, instead of SOAP, you need to to specify
in the command line :option:`-r` or :option:`--rest`


For example, in Bash you'd use::

    export USERNAME=yourname
    export PASSWORD=yadayadayada
    export WSDL_URL=http(s)://example.com/api/wsdl

From there, all shell commands take the form::

    cornerstone <command> [arguments...]

Run :program:`cornerstone help` to get a full list of all possible commands,
and run :program:`cornerstone help <command>` to get detailed help for that
command.
