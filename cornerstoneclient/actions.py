import logging

from cornerstoneclient import utils

# configure logging
logger = logging.getLogger(__name__)


# Helper function
def print_returnCode(rCode, msg=None):
    if rCode == -1:
        if msg:
            logger.error(msg)
        else:
            logger.error("An exception has been produced in the server")
    elif rCode == 0:
        logger.info("Command executed successfully")
    elif rCode == 1:
        logger.error("There is an error in the validation of the parameters")
    elif rCode == 2:
        logger.error("Project already exists")


def print_result_project(command, rCode, project):
    if not rCode:
        if project:
            utils.print_dict(dict(project))
        else:
            logger.info("%s executed successfully", command)
    else:
        logger.error("%s failed with return code '%d'", command, rCode)
        print_returnCode(rCode)


# Start of command line actions
@utils.arg('id', metavar='<id>', help='ID of user in FIM')
@utils.arg('login', metavar='<login>', help='ID of user in OpenStack')
def do_person_add(client, args):
    """(REST only) Add new person."""
    printer = client.get_printer()
    ret = client.person_add(fim_id=args.id, login=args.login)
    printer.print_person_add(ret)


@utils.arg('id', metavar='<id>', help='ID of user in FIM')
def do_person_delete(client, args):
    """(REST only) Delete person."""
    printer = client.get_printer()
    ret = client.person_delete(id=args.id)
    printer.print_person_delete(ret)


@utils.arg('id', metavar='<id>', help='ID of user in FIM')
def do_person_get(client, args):
    """(REST only) Get information about person."""
    printer = client.get_printer()
    ret = client.person_get(id=args.id)
    printer.print_person_get(ret)


def do_person_list(client, args):
    """(REST only) List all persons."""
    printer = client.get_printer()
    ret = client.person_list()
    printer.print_person_list(ret)


@utils.arg('id', metavar='<id>', help='ID of user in FIM')
@utils.arg('login', metavar='<login>', help='ID of user in OpenStack')
def do_person_update(client, args):
    """(REST only) Update person information."""
    printer = client.get_printer()
    ret = client.person_update(id=args.id, login=args.login)
    printer.print_person_update(ret)


@utils.arg('--service', metavar='<service>', required=True,
           help='Service name, ex: "heat"')
@utils.arg('--enabled', metavar='<true|false>', required=True,
           help='To enable or disable the service for the project')
@utils.arg('project', metavar='<project>',
           help='ID of project to enable/disable the service')
def do_allow_service(client, args):
    """Allow a service in a project."""
    printer = client.get_printer()
    enabled = utils.string_to_bool(args.enabled)
    ret = client.allow_service(args.service, enabled, args.project)
    printer.print_allow_service(ret)


def do_domain_list(client, args):
    """List all domains."""
    printer = client.get_printer()
    ret = client.domain_list()
    printer.print_domain_list(ret)


def do_service_list(client, args):
    """List all services."""
    printer = client.get_printer()
    ret = client.service_list()
    printer.print_service_list(ret)


@utils.arg('property', metavar='<key=val>',
           help='Property to add to project (key=val format)')
@utils.arg('project', metavar='<project-id>',
           help='Project id to set the property')
def do_project_properties_add(client, args):
    """(REST only) Add new property to project."""
    if "=" not in args.property:
        logger.error('Error validating property. Format is: key=value')
        return 1
    property_name, property_value = args.property.split("=")
    if property_name is None or property_value is None:
        logger.error('Error validating property. Format is: key=value')
        return 1
    printer = client.get_printer()
    ret = client.project_properties_add(args.project,
                                        property_name,
                                        property_value)
    printer.print_project_properties_add(ret)


@utils.arg('property', metavar='<key>',
           help='Property name to delete from project metadata')
@utils.arg('project', metavar='<project-id>',
           help='Project id to delete the property')
def do_project_properties_delete(client, args):
    """(REST only) Delete project property."""
    printer = client.get_printer()
    ret = client.project_properties_delete(args.project, args.property)
    printer.print_project_properties_delete(ret)


@utils.arg('project', metavar='<project-id>',
           help='Project id to list the properties')
def do_project_properties_list(client, args):
    """(REST only) List project properties."""
    printer = client.get_printer()
    ret = client.project_properties_list(args.project)
    printer.print_project_properties_list(ret)


@utils.arg('property', metavar='<key=val>',
           help='Property to update in project (key=val format)')
@utils.arg('project', metavar='<project-id>',
           help='Project id to update the property')
def do_project_properties_update(client, args):
    """(REST only) Update property to project."""
    if "=" not in args.property:
        logger.error('Error validating property. Format is: key=value')
        return 1
    property_name, property_value = args.property.split("=")
    if property_name is None or property_value is None:
        logger.error('Error validating property. Format is: key=value')
        return 1
    printer = client.get_printer()
    ret = client.project_properties_update(args.project,
                                           property_name,
                                           property_value)
    printer.print_project_properties_update(ret)


def do_project_list(client, args):
    """List all projects."""
    printer = client.get_printer()
    ret = client.project_list()
    printer.print_project_list(ret)


@utils.arg('project', metavar='<project>', help='ID of project to display')
def do_project_get(client, args):
    """Display project details."""
    printer = client.get_printer()
    ret = client.project_get_info(args.project)
    printer.print_project_get(ret)


@utils.arg('--id', metavar='<project-id>', required=True,
           help='New project id (must be unique)')
@utils.arg('--name', metavar='<project-name>', required=True,
           help='New project name (must be unique)')
@utils.arg('--description', metavar='<project-description>', default='',
           help='Description of new project (default is none)')
@utils.arg('--enabled', metavar='<true|false>', default=True,
           help='Initial project enabled status (default true)')
@utils.arg('--owner', metavar='<owner>', required=True,
           help='Owner of the project')
@utils.arg('--administrator', metavar='<administrator>', default=None,
           help='administrator group of the project (default None)')
@utils.arg('--status', metavar='<status>', default='active',
           help='Status of the project (default active)')
@utils.arg('--type', metavar='<status>', default='official',
           choices=['official', 'personal'],
           help='Type of the project (default official)')
def do_project_create(client, args):
    """Create new project."""
    printer = client.get_printer()
    ret = client.project_create(id=args.id,
                                name=args.name,
                                description=args.description,
                                enabled=utils.string_to_bool(args.enabled),
                                owner=args.owner,
                                administrator=args.administrator,
                                status=args.status,
                                type=args.type)
    printer.print_project_create(ret)


@utils.arg('project', metavar='<project-id>',
           help='Project id to get service list')
def do_project_service_list(client, args):
    """(REST only) Returns the list of services enabled for a project."""
    printer = client.get_printer()
    ret = client.project_service_list(project_id=args.project)
    printer.print_project_service_list(ret)


@utils.arg('--name', metavar='<project_name>',
           help='Desired new name of project')
@utils.arg('--description', metavar='<project-description>', default=None,
           help='Desired new description of project')
@utils.arg('--enabled', metavar='<true|false>', default=True,
           help='Enable or disable project')
@utils.arg('--owner', metavar='<owner_uid>',
           help='UID of the desired owner of the project')
@utils.arg('--administrator', metavar='<administrator_uid>',
           help='UID of the desired administrator of the project')
@utils.arg('--status', metavar='<status>', default='active',
           help='Status of the project (default active)')
@utils.arg('--type', metavar='<status>', default='official',
           choices=['official', 'personal'],
           help='Type of the project (default official)')
@utils.arg('project', metavar='<project>', help='ID of project to update')
def do_project_update(client, args):
    """Update project name, description, enabled status, owner and admin"""
    kwargs = {}
    kwargs.update({'id': args.project})
    if args.name:
        kwargs.update({'name': args.name})
    if args.description is not None:
        kwargs.update({'description': args.description})
    if args.enabled:
        kwargs.update({'enabled': utils.string_to_bool(args.enabled)})
    if args.owner:
        kwargs.update({'owner': args.owner})
    if args.administrator:
        kwargs.update({'administrator': args.administrator})
    if args.status:
        kwargs.update({'status': args.status})
    if args.type:
        kwargs.update({'type': args.type})
    if kwargs == {}:
        logger.error("Project not updated, no arguments present.")
        return
    printer = client.get_printer()
    ret = client.project_update(**kwargs)
    printer.print_project_update(ret)


@utils.arg('project', metavar='<project>', help='ID of project to delete')
def do_project_delete(client, args):
    """Delete project."""
    confirm = input("Are you sure to delete the project '{0}'? "
                    "(Doing it outside FIM will make FIM create it "
                    "again later on) '{0}'? "
                    "(y/n): ".format(args.project))
    if confirm in ['y', 'Y', 'yes', 'Yes', 'YES']:
        printer = client.get_printer()
        ret = client.project_delete(args.project)
        printer.print_project_delete(ret)
    else:
        logger.error("Project deletion cancelled by user")


@utils.arg('--rolename', metavar='<role-name>', required=True,
           help='name of the role to update')
@utils.arg('--members', metavar='<member1,member2,...>', required=True,
           help='comma-separated string with the users or groups')
@utils.arg('project', metavar='<project>', help='ID of project to update')
def do_project_members_add(client, args):
    """Add members from the array on the role on a given project."""
    printer = client.get_printer()
    ret = client.add_project_members(id=args.project,
                                     rolename=args.rolename,
                                     members=args.members)
    printer.print_add_project_members(ret)


@utils.arg('--rolename', metavar='<role-name>', required=True,
           help='name of the role to update')
@utils.arg('--members', metavar='<member1,member2,...>', required=True,
           help='comma-separated string with the users or groups')
@utils.arg('project', metavar='<project>', help='ID of project to update')
def do_project_members_del(client, args):
    """Delete the members from the array on the role on a given project."""
    printer = client.get_printer()
    ret = client.del_project_members(id=args.project,
                                     rolename=args.rolename,
                                     members=args.members)
    printer.print_del_project_members(ret)


@utils.arg('--rolename', metavar='<role-name>', required=True,
           help='name of the role to update')
@utils.arg('--members', metavar='<member1,member2,...>', required=True,
           help='comma-separated string with the users or groups')
@utils.arg('project', metavar='<project>', help='ID of project to update')
def do_project_members_set(client, args):
    """Set the members from the array on the role on a given project."""
    printer = client.get_printer()
    ret = client.set_project_members(id=args.project,
                                     rolename=args.rolename,
                                     members=args.members)
    printer.print_set_project_members(ret)


@utils.arg('--rolename', metavar='<role-name>', required=True,
           help='name of the role to update')
@utils.arg('project', metavar='<project>', help='ID of project to update')
def do_project_members_get(client, args):
    """Get the members from the array on the role on a given project."""
    printer = client.get_printer()
    ret = client.get_project_members(id=args.project,
                                     rolename=args.rolename)
    printer.print_get_project_members(ret)


@utils.arg('project', metavar='<project>', help='ID of project to display')
def do_project_get_info(client, args):
    """Display project extra metadata."""
    printer = client.get_printer()
    ret = client.project_get_info(args.project)
    printer.print_project_get(ret)


@utils.arg('--value', metavar='<true|false>', required=True,
           help='Enable or disable')
@utils.arg('project', metavar='<project>', help='ID of project to update')
def do_allow_operators(client, args):
    """Enable/disable operators access on a project."""
    kwargs = {}
    kwargs.update({'id': args.project})
    if args.value:
        kwargs.update({'value': utils.string_to_bool(args.value)})
    if kwargs == {}:
        logger.error("Project not updated, no arguments present.")
        return
    printer = client.get_printer()
    ret = client.project_allow_operators(**kwargs)
    printer.print_project_allow_operators(ret)


@utils.arg('--value', metavar='<true|false>', required=True,
           help='Enable or disable')
@utils.arg('project', metavar='<project>', help='ID of project to update')
def do_allow_supporters(client, args):
    """(REST Only) Enable/disable supporters access on a project."""
    kwargs = {}
    kwargs.update({'id': args.project})
    if args.value:
        kwargs.update({'value': utils.string_to_bool(args.value)})
    if kwargs == {}:
        logger.error("Project not updated, no arguments present.")
        return
    printer = client.get_printer()
    ret = client.project_allow_supporters(**kwargs)
    printer.print_project_allow_supporters(ret)


def do_schema_get(client, args):
    """(REST only) Get all model schemas."""
    printer = client.get_printer()
    ret = client.schema_get()
    printer.print_schema_get(ret)
