#!/usr/bin/python

import argparse
import getpass
import logging
import os
import sys

from cornerstoneclient import actions
from cornerstoneclient import exceptions as exc
from cornerstoneclient.rest.v1 import ClientRestV1
from cornerstoneclient.rest.v2 import ClientRestV2
from cornerstoneclient.soap.client import ClientSoap

from cornerstoneclient import utils

# configure logging
logging.getLogger('requests').setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

CONSOLE_MESSAGE_FORMAT = '%(levelname)s: %(message)s'
dump_stack_trace = False

rest_clients = {
    '1': ClientRestV1,
    '2': ClientRestV2
}


def env(*vars, **kwargs):
    """Search for the first defined of possibly many env vars.

    Returns the first environment variable defined in vars, or
    returns the default defined in kwargs.

    """
    for v in vars:
        value = os.environ.get(v, None)
        if value:
            return value
    return kwargs.get('default', '')


class CornerstoneShell(object):

    def get_base_parser(self):
        parser = argparse.ArgumentParser(
            prog='cornerstone',
            description='Cornerstone command line interface',
            epilog='See "cornerstone help COMMAND" '
                   'for help on a specific command.',
            add_help=False,
            formatter_class=CornerstoneHelpFormatter,
        )

        # Global arguments
        parser.add_argument('-v',
                            '--verbose',
                            action='count',
                            dest='verbose_level',
                            default=0,
                            help='Increase verbosity of output.'
                                 'Can be repeated.')
        parser.add_argument('-d',
                            '--debug',
                            help='increase output '
                                 'to debug messages',
                            action='store_true')
        parser.add_argument('-r',
                            '--restful',
                            default=False,
                            action='store_true',
                            help='Using RESTful calls')

        parser.add_argument('--restful-version',
                            default='1',
                            help='Version for the RESTful calls')

        parser.add_argument('--auth',
                            default='basic',
                            choices=['basic', 'kerberos', 'oidc'],
                            help='Defines the authentication to use')

        parser.add_argument('--client-id',
                            metavar='<auth-client-id',
                            default=env('CLIENT_ID'),
                            help='Client ID for OIDC, '
                                 'defaults to env[CLIENT_ID]')

        parser.add_argument('--client-secret',
                            metavar='<auth-client-secret',
                            default=env('CLIENT_SECRET'),
                            help='Client Secret for OIDC, '
                                 'defaults to env[CLIENT_SECRET]')
        parser.add_argument('--token-url',
                            metavar='<auth-token-url',
                            default=env('TOKEN_URL'),
                            help='TokenURL for OIDC'
                                 'defaults to env[TOKEN_URL]')
        parser.add_argument('--audience',
                            metavar='<auth-audience>',
                            default=env('AUDIENCE'),
                            help='Audience for OIDC, '
                                 'defaults to env[AUDIENCE]')

        parser.add_argument('--username',
                            metavar='<auth-user-name>',
                            default=env('USERNAME'),
                            help='Defaults to env[USERNAME]')

        parser.add_argument('--password',
                            metavar='<auth-password>',
                            default=env('PASSWORD'),
                            help='Defaults to env[PASSWORD]')

        parser.add_argument('--url',
                            metavar='<wsdl-url>',
                            default=env('WSDL_URL'),
                            help='Defaults to env[WSDL_URL]')

        parser.add_argument('--cert',
                            metavar='<ca-bundle>',
                            default=None,
                            help='Defaults to None')
        return parser

    def get_subcommand_parser(self):
        parser = self.get_base_parser()

        self.subcommands = {}
        subparsers = parser.add_subparsers(metavar='<subcommand>')

        self._find_actions(subparsers, actions)
        self._find_actions(subparsers, self)

        return parser

    def _find_actions(self, subparsers, actions_module):
        for attr in (a for a in dir(actions_module) if a.startswith('do_')):
            # I prefer to be hypen-separated instead of underscores.
            command = attr[3:].replace('_', '-')
            callback = getattr(actions_module, attr)
            desc = callback.__doc__ or ''
            help = desc.strip().split('\n')[0]
            arguments = getattr(callback, 'arguments', [])
            subparser = subparsers.add_parser(
                command,
                help=help,
                description=desc,
                add_help=False,
                formatter_class=CornerstoneHelpFormatter)
            subparser.add_argument('-h', '--help', action='help',
                                   help=argparse.SUPPRESS)
            self.subcommands[command] = subparser
            for (args, kwargs) in arguments:
                subparser.add_argument(*args, **kwargs)
            subparser.set_defaults(func=callback)

    def configure_logging(self, args):
        """Typical app logging setup."""
        global dump_stack_trace
        root_logger = logging.getLogger('')
        # Always send higher-level messages to the console via stderr
        console = logging.StreamHandler(sys.stderr)
        formatter = logging.Formatter(CONSOLE_MESSAGE_FORMAT)
        console.setFormatter(formatter)
        root_logger.addHandler(console)
        # Set logging to the requested level
        dump_stack_trace = False
        if args.verbose_level >= 3:
            # Three or more --verbose
            root_logger.setLevel(logging.DEBUG)
        elif args.verbose_level == 2:
            # Two --verbose
            root_logger.setLevel(logging.INFO)
        elif args.verbose_level == 1:
            # One --verbose
            root_logger.setLevel(logging.WARNING)
        else:
            # This is the default case, no --debug, --verbose
            root_logger.setLevel(logging.ERROR)
        if args.debug:
            # --debug forces traceback
            dump_stack_trace = True
            root_logger.setLevel(logging.DEBUG)

    def main(self, argv):
        # Parse args once to find version
        parser = self.get_base_parser()
        (options, args) = parser.parse_known_args(argv)

        # build available subcommands based on version
        subcommand_parser = self.get_subcommand_parser()
        self.parser = subcommand_parser

        # Handle top-level --help/-h before attempting to parse
        # a command off the command line
        if not argv or hasattr(options, 'help'):
            self.do_help(options)
            return 0

        # Parse args again and call whatever callback was selected
        args = subcommand_parser.parse_args(argv)

        # Short-circuit and deal with help command right away.
        if args.func == self.do_help:
            self.do_help(args)
            return 0

        self.configure_logging(args)

        if args.auth == 'basic':
            if not args.username:
                raise exc.CommandError(
                    'Expecting a username provided via either '
                    '--username or env[USERNAME]')

            if not args.password:
                # No password, If we've got a tty, try prompting for it
                if hasattr(sys.stdin, 'isatty') and sys.stdin.isatty():
                    # Check for Ctl-D
                    try:
                        args.password = getpass.getpass('Password: ')
                    except EOFError:
                        pass
                # No password because we did't have a tty or the
                # user Ctl-D when prompted?
                if not args.password:
                    raise exc.CommandError(
                        'Expecting a password provided via either '
                        '--password, env[PASSWORD], or '
                        'prompted response')
            auth = 'basic'
            params = {
                'username': args.username,
                'password': args.password,
            }
        elif args.auth == 'kerberos':
            auth = 'kerberos'
            params = {}
        elif args.auth == 'oidc':
            if not args.client_id:
                raise exc.CommandError(
                    'Expecting a client id provided via either '
                    '--client-id or env[CLIENT_ID]')
            if not args.client_secret:
                # No password, If we've got a tty, try prompting for it
                if hasattr(sys.stdin, 'isatty') and sys.stdin.isatty():
                    # Check for Ctl-D
                    try:
                        args.client_secret = getpass.getpass('Client Secret: ')
                    except EOFError:
                        pass
                # No password because we did't have a tty or the
                # user Ctl-D when prompted?
                if not args.client_secret:
                    raise exc.CommandError(
                        'Expecting a client secret provided via either '
                        '--client-secret, env[CLIENT_SECRET], or '
                        'prompted response')
            if not args.audience:
                raise exc.CommandError(
                    'Expecting a audience provided via either '
                    '--audience or env[AUDIENCE]')
            if not args.token_url:
                raise exc.CommandError(
                    'Expecting a token_url provided via either '
                    '--token-url or env[TOKEN_URL]')
            auth = 'oidc'
            params = {
                'id': args.client_id,
                'secret': args.client_secret,
                'audience': args.audience,
                'token_url': args.token_url,
            }
        if not args.url:
            raise exc.CommandError(
                'Expecting a url provided via either '
                '--url or env[WSDL-URL]')

        if args.restful:
            self.client = rest_clients[args.restful_version](
                auth=auth,
                params=params,
                url=args.url,
                debug=args.debug,
                cert=args.cert)
        else:
            self.client = ClientSoap(
                auth=auth,
                params=params,
                url=args.url,
                debug=args.debug,
                cert=args.cert)
        args.func(self.client, args)

    @utils.arg('command', metavar='<subcommand>', nargs='?',
               help='Display help for <subcommand>')
    def do_help(self, args):
        """Display help about this program or one of its subcommands."""
        if getattr(args, 'command', None):
            if args.command in self.subcommands:
                self.subcommands[args.command].print_help()
            else:
                raise exc.CommandError("'%s' is not a valid subcommand" %
                                       args.command)
        else:
            self.parser.print_help()


class CornerstoneHelpFormatter(argparse.HelpFormatter):
    def start_section(self, heading):
        # Title-case the headings
        heading = '%s%s' % (heading[0].upper(), heading[1:])
        super(CornerstoneHelpFormatter, self).start_section(heading)


def main():
    try:
        CornerstoneShell().main(sys.argv[1:])

    except Exception as e:
        logger.exception(e)
        sys.exit(1)


if __name__ == "__main__":
    sys.exit(main())
