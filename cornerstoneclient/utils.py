import prettytable


# Decorator for cli-args
def arg(*args, **kwargs):
    def _decorator(func):
        # Because of the sematics of decorator composition if we just append
        # to the options list positional options will appear to be backwards.
        func.__dict__.setdefault('arguments', []).insert(0, (args, kwargs))
        return func
    return _decorator


def pretty_choice_list(choices):
    return ', '.join("'%s'" % i for i in choices)


def print_list(objs, fields, formatters={}, order_by=None):
    table = prettytable.PrettyTable([f for f in fields], caching=False)
    table.aligns = ['l' for f in fields]

    for o in objs:
        row = []
        for field in fields:
            if field in formatters:
                row.append(formatters[field](o))
            else:
                field_name = field.lower().replace(' ', '_')
                data = getattr(o, field_name, '')
                if data is None:
                    data = ''
                row.append(data)
        table.add_row(row)

    if order_by is None:
        order_by = fields[0]
    print(table.get_string(sortby=order_by))


def _word_wrap(string, max_length=0):
    """wrap long strings to be no longer then max_length."""
    if max_length <= 0:
        return string
    return '\n'.join([string[i:i + max_length] for i in
                     range(0, len(string), max_length)])


def print_dict(d, wrap=36):
    """pretty table prints dictionaries.

    Wrap values to max_length wrap if wrap>0
    """
    table = prettytable.PrettyTable(['Property', 'Value'], caching=False)
    table.aligns = ['l', 'l']
    for (prop, value) in iter(d.items()):
        if value is None:
            value = ''
        # else, if 'value' is a List (from soap)
        elif hasattr(value, 'item'):
            value = ', '.join(value.item)
        elif isinstance(value, list):
            value = ', '.join(value)
        value = _word_wrap(str(value), max_length=wrap)
        table.add_row([prop, value])
    print(table.get_string(sortby='Property'))


def print_dict_list(dict_list, fields=None, wrap=0):
    if not dict_list:
        print("No content to list")
        return True

    if not fields:
        fields = dict_list[0].keys()

    table = prettytable.PrettyTable([f for f in fields], caching=False)
    table.aligns = ['l' for f in fields]

    for d in dict_list:
        row = []
        for value in d.values():
            if value is None:
                value = ''
            elif hasattr(value, 'item'):
                value = ', '.join(value.item)
            if not isinstance(value, bool):
                value = _word_wrap(value, max_length=wrap)
            row.append(value)
        table.add_row(row)
    print(table)


def print_schema(schema_list):
    if not schema_list:
        return
    fields = schema_list[0].keys()

    table = prettytable.PrettyTable([f for f in fields], caching=False)
    table.aligns = ['l' for f in fields]

    for dict_item in schema_list:
        row = []
        row.append(dict_item['name'])
        attrs = ''
        for attr_dict_item in dict_item['properties']:
            attrs += str(attr_dict_item)
        row.append(attrs)
        table.add_row(row)
    print(table)


def string_to_bool(arg):
    if isinstance(arg, bool):
        return arg
    return arg.strip().lower() in ('t', 'true', 'yes', '1')
