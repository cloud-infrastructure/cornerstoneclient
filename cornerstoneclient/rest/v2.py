import json
import logging
import warnings

from cornerstoneclient.client import Client
from cornerstoneclient.rest.printer import PrinterRest

# configure logging
logger = logging.getLogger(__name__)

API_VERSION = '/v2'

ENDPOINTS = {
    'project_list': '/project',
    'project_get': '/project/{0}',
    'schema': '/schema',
}


class ClientRestV2(Client):

    # Base endpoint
    base_url = ''

    def __init__(self, auth=None, params={}, url=None, debug=False, cert=None):
        super(ClientRestV2, self).__init__(auth, params, url, debug, cert)
        self._set_base_url()
        self.session = self.get_session()

    def _build_url(self, url, params=None):
        if params:
            url = url.format(*params)
        return self.base_url + url

    def _dispatch(self, url, params=None, data=None, method='GET'):
        headers = {'Content-Type': 'application/json'}
        url = self._build_url(API_VERSION + url, params)
        method = method.upper()
        # See https://github.com/shazow/urllib3/issues/497
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            return self.session.request(
                method=method, url=url, data=data,
                headers=headers)

    def _set_base_url(self):
        chunks = self.url.split('/')
        self.base_url = '{0}//{1}'.format(chunks[0], chunks[2])

    def get_printer(self):
        """Return an object of the appropiate class to print the results."""
        return PrinterRest()

    def allow_service(self, service_name, enabled, project_id):
        raise NotImplementedError

    def domain_list(self):
        raise NotImplementedError

    def service_list(self):
        raise NotImplementedError

    def person_add(self, id, login):
        raise NotImplementedError

    def person_delete(self, id):
        raise NotImplementedError

    def person_get(self, id):
        raise NotImplementedError

    def person_list(self):
        raise NotImplementedError

    def person_update(self, id, login):
        raise NotImplementedError

    def add_project_members(self, id, rolename, members):
        raise NotImplementedError

    def del_project_members(self, id, rolename, members):
        raise NotImplementedError

    def set_project_members(self, id, rolename, members):
        raise NotImplementedError

    def get_project_members(self, id, rolename):
        raise NotImplementedError

    def project_allow_operators(self, id, value=None):
        raise NotImplementedError

    def project_allow_supporters(self, id, value=None):
        raise NotImplementedError

    def project_create(self, id, name, description, owner,
                       administrator, status, type, enabled=None):
        """Create new project."""
        data = {
            "id": id,
            "name": name,
            "description": description,
            "status": status,
            "type": type,
            "owner": owner,
            "administrator": administrator
        }
        data = json.dumps(data)
        return self._dispatch(ENDPOINTS['project_list'], data=data,
                              method='POST')

    def project_delete(self, id):
        """Delete tenant."""
        return self._dispatch(ENDPOINTS['project_get'], params=[id],
                              method='DELETE')

    def project_get(self, project_id):
        """Display project details."""
        return self._dispatch(ENDPOINTS['project_get'],
                              params=[project_id])

    def project_get_info(self, project_id):
        return self.project_get(project_id)

    def project_list(self):
        """List all projects."""
        return self._dispatch(ENDPOINTS['project_list'])

    def project_update(self, id, name=None, description=None, enabled=None,
                       owner=None, administrator=None, status=None, type=None):
        """Update project name, description, enabled status, owner."""
        data = {}
        if name:
            data['name'] = name
        if description:
            data['description'] = description
        if status:
            data['status'] = status
        if type:
            data['type'] = type
        if owner:
            data['owner'] = owner
        if administrator:
            data['administrator'] = administrator
        data = json.dumps(data)
        return self._dispatch(ENDPOINTS['project_get'], data=data,
                              method='PUT', params=[id])

    def project_properties_add(self, project_id, property_name,
                               property_value):
        raise NotImplementedError

    def project_properties_delete(self, project_id, property_name):
        raise NotImplementedError

    def project_properties_list(self, project_id):
        raise NotImplementedError

    def project_properties_update(self, project_id, property_name,
                                  property_value):
        raise NotImplementedError

    def project_service_list(self, project_id):
        """Return the list of services enabled for a project."""
        raise NotImplementedError

    def schema_get(self):
        return self._dispatch(ENDPOINTS['schema'])
