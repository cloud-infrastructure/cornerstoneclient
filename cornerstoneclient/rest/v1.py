import json
import logging
import warnings

from cornerstoneclient.client import Client
from cornerstoneclient.rest.printer import PrinterRest

# configure logging
logger = logging.getLogger(__name__)

API_VERSION = '/v1'

ENDPOINTS = {
    'project_list': '/projects/',
    'project_services': '/projects/{0}/services/',
    'project_properties': '/projects/{0}/properties/',
    'project_properties_ud': '/projects/{0}/properties/{1}/',
    'domain_list': '/domains/',
    'service_list': '/services/',
    'project_get': '/projects/{0}/',
    'add_project_members': '/projects/{0}/assignments/{1}/',
    'delete_project_members': '/projects/{0}/assignments/{1}/',
    'get_project_members': '/projects/{0}/assignments/{1}/',
    'allow': '/projects/{0}/grants/',
    'person_list': '/persons/',
    'person_get': '/persons/{0}/',
    'schema': '/schemas/',
}


class ClientRestV1(Client):

    # Base endpoint
    base_url = ''

    def __init__(self, auth=None, params={}, url=None, debug=False, cert=None):
        super(ClientRestV1, self).__init__(auth, params, url, debug, cert)
        self._set_base_url()

        self.session = self.get_session()

    def _build_url(self, url, params=None):
        if params:
            url = url.format(*params)
        return self.base_url + url

    def _dispatch(self, url, params=None, data=None, method='GET'):
        headers = {'Content-Type': 'application/json'}
        url = self._build_url(API_VERSION + url, params)
        method = method.upper()
        # See https://github.com/shazow/urllib3/issues/497
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            return self.session.request(method=method, url=url, data=data,
                                        headers=headers)

    def _get_json_data_for_members_actions(self, project_id, rolename,
                                           members):
        member_list = []
        for m in members.split(','):
            member_list.append({"name": m})
        data = {'members': member_list}
        return json.dumps(data)

    def _set_base_url(self):
        chunks = self.url.split('/')
        self.base_url = '{0}//{1}'.format(chunks[0], chunks[2])

    def get_printer(self):
        """Return an object of the appropiate class to print the results."""
        return PrinterRest()

    def allow_service(self, service_name, enabled, project_id):
        """Allow a service in a project."""
        data = {"service_name": service_name}
        data = json.dumps(data)
        method = 'POST'
        if not enabled:
            method = 'DELETE'
        return self._dispatch(ENDPOINTS['project_services'],
                              params=[project_id],
                              data=data,
                              method=method)

    def domain_list(self):
        """List all domains."""
        return self._dispatch(ENDPOINTS['domain_list'])

    def service_list(self):
        """List all services."""
        return self._dispatch(ENDPOINTS['service_list'])

    def person_add(self, id, login):
        """Add new person."""
        data = {"id": id, "login": login}
        data = json.dumps(data)
        return self._dispatch(ENDPOINTS['person_list'], data=data,
                              method='POST')

    def person_delete(self, id):
        """Delete person by id."""
        return self._dispatch(ENDPOINTS['person_get'], params=[id],
                              method='DELETE')

    def person_get(self, id):
        """Get person info by id."""
        return self._dispatch(ENDPOINTS['person_get'], params=[id])

    def person_list(self):
        """List all persons."""
        return self._dispatch(ENDPOINTS['person_list'])

    def person_update(self, id, login):
        """Update person information."""
        data = {"id": id, "login": login}
        data = json.dumps(data)
        return self._dispatch(ENDPOINTS['person_get'], params=[id],
                              data=data, method='PUT')

    def add_project_members(self, id, rolename, members):
        """Add the members from the array on the role on a given project."""
        data = self._get_json_data_for_members_actions(id, rolename, members)
        return self._dispatch(ENDPOINTS['add_project_members'], data=data,
                              method='POST', params=[id, rolename])

    def del_project_members(self, id, rolename, members):
        """Delete the members from the array on the role on a given project."""
        data = self._get_json_data_for_members_actions(id, rolename, members)
        return self._dispatch(ENDPOINTS['delete_project_members'], data=data,
                              method='DELETE', params=[id, rolename])

    def set_project_members(self, id, rolename, members):
        """Set the members from the array on the role on a given project."""
        data = self._get_json_data_for_members_actions(id, rolename, members)
        return self._dispatch(ENDPOINTS['add_project_members'], data=data,
                              method='PUT', params=[id, rolename])

    def get_project_members(self, id, rolename):
        """List the members on a role on an specific project."""
        return self._dispatch(ENDPOINTS['get_project_members'],
                              params=[id, rolename])

    def project_allow_operators(self, id, value=None):
        """Set the value to the allow operators field."""
        data = {"group_name": "operators"}
        data = json.dumps(data)
        method = 'POST'
        if not value:
            method = 'DELETE'

        return self._dispatch(ENDPOINTS['allow'], data=data, method=method,
                              params=[id])

    def project_allow_supporters(self, id, value=None):
        """Set the value to the allow supporters field."""
        data = {"group_name": "supporters"}
        data = json.dumps(data)
        method = 'POST'
        if not value:
            method = 'DELETE'

        return self._dispatch(ENDPOINTS['allow'], data=data, method=method,
                              params=[id])

    def project_create(self, id, name, description, enabled, owner,
                       administrator, status, type):
        """Create new project."""
        data = {"id": id, "name": name, "description": description,
                "enabled": enabled, "owner": owner}
        data = json.dumps(data)
        return self._dispatch(ENDPOINTS['project_list'], data=data,
                              method='POST')

    def project_delete(self, id):
        """Delete tenant."""
        return self._dispatch(ENDPOINTS['project_get'], params=[id],
                              method='DELETE')

    def project_get(self, project_id):
        """Display project details."""
        return self._dispatch(ENDPOINTS['project_get'],
                              params=[project_id])

    def project_get_info(self, id):
        """Retrieve the extra data from a project."""
        return self.project_get(id)

    def project_list(self):
        """List all projects."""
        return self._dispatch(ENDPOINTS['project_list'])

    def project_update(self, id, name=None, description=None, enabled=None,
                       owner=None, administrator=None, status=None, type=None):
        """Update project name, description, enabled status, owner."""
        data = {}
        if name:
            data['name'] = name
        if description:
            data['description'] = description
        if enabled is not None:
            data['enabled'] = enabled
        if owner:
            data['owner'] = owner
        data = json.dumps(data)
        return self._dispatch(ENDPOINTS['project_get'], data=data,
                              method='PUT', params=[id])

    def project_properties_add(self, project_id, property_name,
                               property_value):
        data = {'property_name': property_name,
                'property_value': property_value}
        data = json.dumps(data)
        params = [project_id]
        return self._dispatch(ENDPOINTS['project_properties'],
                              method='POST',
                              data=data,
                              params=params)

    def project_properties_delete(self, project_id, property_name):
        params = [project_id, property_name]
        return self._dispatch(ENDPOINTS['project_properties_ud'],
                              method='DELETE',
                              params=params)

    def project_properties_list(self, project_id):
        params = [project_id]
        return self._dispatch(ENDPOINTS['project_properties'],
                              params=params)

    def project_properties_update(self, project_id, property_name,
                                  property_value):
        data = {'property_name': property_name,
                'property_value': property_value}
        data = json.dumps(data)
        params = [project_id, property_name]
        return self._dispatch(ENDPOINTS['project_properties_ud'],
                              method='PUT',
                              data=data,
                              params=params)

    def project_service_list(self, project_id):
        """Return the list of services enabled for a project."""
        return self._dispatch(ENDPOINTS['project_services'],
                              params=[project_id])

    def schema_get(self):
        return self._dispatch(ENDPOINTS['schema'])
