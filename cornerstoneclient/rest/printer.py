import json
import logging

from cornerstoneclient.utils import print_dict
from cornerstoneclient.utils import print_dict_list
from cornerstoneclient.utils import print_schema

# configure logging
logger = logging.getLogger(__name__)


class PrinterRest(object):

    def _check_status_code(self, ret):
        # Check for error status from server
        if 400 <= ret.status_code <= 511:
            # Some kind of error from server
            message = '({0})'.format(ret.reason)
            try:
                jdata = json.loads(ret.text)
                # Server always send error message inside 'detail'
                if 'detail' in jdata:
                    message += ' ' + jdata['detail']
            except ValueError:
                message += ' No detailed information about the error.'
            raise Exception(message)

    def _print_list(self, ret, fields=None):
        try:
            self._check_status_code(ret)
            data = json.loads(ret.text)
            if 'data' in data:
                data = data['data']
            print_dict_list(data, fields=fields, wrap=60)
        except Exception as e:
            logger.exception(e)

    def _print_no_content(self, command, ret):
        try:
            self._check_status_code(ret)
            logger.info("%s executed successful", command)
        except Exception as e:
            logger.exception(e)

    def _print_simple_json(self, ret):
        try:
            self._check_status_code(ret)
            data = json.loads(ret.text)
            if 'data' in data:
                data = data['data']
            print_dict(data)
        except Exception as e:
            logger.exception(e)

    def print_allow_service(self, ret):
        try:
            self._check_status_code(ret)
            self._print_no_content("Allow/disallow service", ret)
        except Exception as e:
            logger.exception(e)

    def print_allow_operators(self, ret):
        try:
            self._check_status_code(ret)
            self._print_no_content("Allow/disallow operators", ret)
        except Exception as e:
            logger.exception(e)

    def print_allow_supporters(self, ret):
        try:
            self._check_status_code(ret)
            self._print_no_content("Allow/disallow supporters", ret)
        except Exception as e:
            logger.exception(e)

    def print_domain_list(self, ret):
        try:
            self._check_status_code(ret)
            self._print_list(ret)
        except Exception as e:
            logger.exception(e)

    def print_service_list(self, ret):
        try:
            self._check_status_code(ret)
            self._print_list(ret)
        except Exception as e:
            logger.exception(e)

    def print_person_add(self, ret):
        self._print_simple_json(ret)

    def print_person_delete(self, ret):
        try:
            self._check_status_code(ret)
            self._print_no_content("Person delete", ret)
        except Exception as e:
            logger.exception(e)

    def print_person_get(self, ret):
        self._print_simple_json(ret)

    def print_person_list(self, ret):
        try:
            self._check_status_code(ret)
            self._print_list(ret)
        except Exception as e:
            logger.exception(e)

    def print_person_update(self, ret):
        self._print_simple_json(ret)

    def print_project_list(self, ret):
        try:
            self._check_status_code(ret)
            self._print_list(ret)
        except Exception as e:
            logger.exception(e)

    def print_project_get(self, ret):
        self._print_simple_json(ret)

    def print_project_get_info(self, ret):
        try:
            self._check_status_code(ret)
            self.print_project_get(ret)
        except Exception as e:
            logger.exception(e)

    def print_project_create(self, ret):
        self._print_simple_json(ret)

    def print_project_update(self, ret):
        self._print_simple_json(ret)

    def print_project_delete(self, ret):
        try:
            self._check_status_code(ret)
            self._print_no_content("Project delete", ret)
        except Exception as e:
            logger.exception(e)

    def print_add_project_members(self, ret):
        try:
            self._check_status_code(ret)
            self._print_no_content("Add members to project", ret)
        except Exception as e:
            logger.exception(e)

    def print_del_project_members(self, ret):
        try:
            self._check_status_code(ret)
            self._print_no_content("Members removed from project", ret)
        except Exception as e:
            logger.exception(e)

    def print_set_project_members(self, ret):
        try:
            self._check_status_code(ret)
            self._print_no_content("Set members to project", ret)
        except Exception as e:
            logger.exception(e)

    def print_get_project_members(self, ret):
        try:
            self._check_status_code(ret)
            self._print_list(ret)
        except Exception as e:
            logger.exception(e)

    def print_project_allow_operators(self, ret):
        try:
            self._check_status_code(ret)
            self._print_no_content('Allow/disallow operators', ret)
        except Exception as e:
            logger.exception(e)

    def print_project_allow_supporters(self, ret):
        try:
            self._check_status_code(ret)
            self._print_no_content('Allow/disallow supporters', ret)
        except Exception as e:
            logger.exception(e)

    def print_project_properties_add(self, ret):
        try:
            self._check_status_code(ret)
            self._print_no_content('Add project property', ret)
        except Exception as e:
            logger.exception(e)

    def print_project_properties_delete(self, ret):
        try:
            self._check_status_code(ret)
            self._print_no_content('Delete project property', ret)
        except Exception as e:
            logger.exception(e)

    def print_project_properties_list(self, ret):
        try:
            self._check_status_code(ret)
            self._print_list(ret)
        except Exception as e:
            logger.exception(e)

    def print_project_properties_update(self, ret):
        try:
            self._check_status_code(ret)
            self._print_no_content('Update project property', ret)
        except Exception as e:
            logger.exception(e)

    def print_project_service_list(self, ret):
        try:
            self._check_status_code(ret)
            self._print_list(ret)
        except Exception as e:
            logger.exception(e)

    def print_schema_get(self, ret):
        try:
            self._check_status_code(ret)
            print_schema(json.loads(ret.text))
        except Exception as e:
            logger.exception(e)
