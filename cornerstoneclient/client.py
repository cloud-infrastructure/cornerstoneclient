from oauthlib.oauth2 import BackendApplicationClient
from requests.auth import HTTPBasicAuth
from requests import Session
from requests_kerberos import DISABLED
from requests_kerberos import HTTPKerberosAuth
from requests_oauthlib import OAuth2Session


class Client(object):
    def __init__(self, auth=None, params={}, url=None, debug=False, cert=None):
        """Construct a new client."""

        self.auth = auth
        self.params = params
        self.url = url
        self.debug = debug
        self.cacert = None
        if cert:
            self.cacert = cert
        self.authenticate()

    def get_session(self):
        if self.auth == 'basic':
            session = Session()
            session.auth = HTTPBasicAuth(
                username=self.params['username'],
                password=self.params['password'])
            return session
        elif self.auth == 'kerberos':
            session = Session()
            session.auth = HTTPKerberosAuth(mutual_authentication=DISABLED)
        elif self.auth == 'oidc':
            client = BackendApplicationClient(
                client_id=self.params['id'])
            session = OAuth2Session(client=client)
            session.fetch_token(
                token_url=self.params['token_url'],
                client_id=self.params['id'],
                client_secret=self.params['secret'],
                audience=self.params['audience'])
        if self.cacert:
            session.verify = self.cacert
        return session

    def authenticate(self):
        """Authenticate the user."""
        # To implement in clients
        pass

    def get_printer(self):
        # To implement in clients
        return None
