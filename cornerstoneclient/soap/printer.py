import logging

from zeep import helpers

from cornerstoneclient.utils import print_dict
from cornerstoneclient.utils import print_list

# configure logging
logger = logging.getLogger(__name__)


# Helper function
def print_returnCode(rCode, msg=None):
    if rCode == -1:
        if msg:
            logger.error(msg)
        else:
            logger.error("An exception has been produced in the server")
    elif rCode == 0:
        logger.info("Command executed successfully")
    elif rCode == 1:
        logger.error("There is an error in the validation of the parameters")
    elif rCode == 2:
        logger.error("Project already exists")


def print_result_project(command, rCode, project):
    if not rCode:
        if project:
            # Fix for WSDL definition with item element on lists
            project.services = project.services.item
            print_dict(helpers.serialize_object(project))
        else:
            logger.info("%s executed successfully", command)
    else:
        logger.error("%s failed with return code '%d'", command, rCode)
        print_returnCode(rCode)


class PrinterSoap(object):

    def _print_result_project(self, command, ret):
        rCode, project = ret
        print_result_project(command, rCode, project)

    def _print_returncode(self, ret):
        rCode, msg = ret
        print_returnCode(rCode, msg)

    def print_allow_service(self, ret):
        self._print_returncode(ret)

    def print_allow_operators(self, ret):
        self._print_returncode(ret)

    def print_domain_list(self, ret):
        rCode, domains = ret
        if not rCode:
            print_list(domains, ['id'], order_by='id')
        else:
            logger.error("domain-list failed with return code '%s'",
                         rCode)
            print_returnCode(rCode)

    def print_project_list(self, ret):
        rCode, projects = ret
        if not rCode:
            print_list(projects, ['id', 'name', 'owner', 'enabled'],
                       order_by='name')
        else:
            logger.error("project-list failed with return code '%s'",
                         rCode)
            print_returnCode(rCode)

    def print_project_get(self, ret):
        self._print_result_project('project-get', ret)

    def print_project_create(self, ret):
        self._print_result_project('project-create', ret)

    def print_project_update(self, ret):
        self._print_result_project('project-update', ret)

    def print_project_delete(self, ret):
        self._print_result_project('project-delete', ret)

    def print_add_project_members(self, ret):
        self._print_returncode(ret)

    def print_del_project_members(self, ret):
        self._print_returncode(ret)

    def print_set_project_members(self, ret):
        self._print_returncode(ret)

    def print_get_project_members(self, ret):
        rCode, members = ret
        if not rCode:
            print_list(members, ['name'], order_by='name')
        else:
            logger.error("get-project-members failed with return code '%d'",
                         rCode)
            print_returnCode(rCode)

    def print_project_get_info(self, ret):
        self._print_result_project('project-get-info', ret)

    def print_project_allow_accounting(self, ret):
        self._print_returncode(ret)

    def print_project_allow_operators(self, ret):
        self._print_returncode(ret)
