import logging

from zeep import Client as zeepClient
from zeep.transports import Transport as zeepTransport

from cornerstoneclient.client import Client
from cornerstoneclient import exceptions as exc
from cornerstoneclient.soap.printer import PrinterSoap

logging.getLogger('suds.client').setLevel(logging.CRITICAL)


class ClientSoap(Client):
    def __init__(self, auth=None, params={}, url=None, debug=False, cert=None):
        super(ClientSoap, self).__init__(auth, params, url, debug, cert)

    def get_printer(self):
        return PrinterSoap()

    def authenticate(self):
        """Authenticate the user."""
        try:
            session = self.get_session()
            self.client = zeepClient(
                self.url,
                transport=zeepTransport(session=session))
        except Exception:
            raise exc.AuthenticationError('Failed to authenticate')

    def allow_service(self, service_name, enabled, project_id):
        """Allow a service in a project."""
        return self.client.service.allow_service(service_name,
                                                 enabled, project_id)

    def domain_list(self):
        """List all domains."""
        ret = self.client.service.domain_list()
        if not ret.returnCode:
            domains = ret.domainList.item
        return ret.returnCode, domains

    def project_list(self):
        """List all projects."""
        pList = None
        ret = self.client.service.project_list()
        if not ret.returnCode:
            pList = ret.projectList.item
        return ret.returnCode, pList

    def project_get(self, project_id):
        """Display project details."""
        project = None
        ret = self.client.service.project_get(project_id)
        if not ret.returnCode:
            project = ret.project
        return ret.returnCode, project

    def project_create(self, id, name, description, enabled, owner,
                       administrator, status, type):
        """Create new project."""
        project = None
        ret = self.client.service.project_create(id, name, description,
                                                 enabled, owner)
        if not ret.returnCode:
            project = ret.project
        return ret.returnCode, project

    def project_update(self, id, name=None, description=None, enabled=None,
                       owner=None, administrator=None, status=None, type=None):
        """Update project name, description, enabled status, owner."""
        project = None
        ret = self.client.service.project_update(id, name, description,
                                                 enabled, owner)
        if not ret.returnCode:
            project = ret.project
        return ret.returnCode, project

    def project_delete(self, id):
        """Delete tenant."""
        project = None
        ret = self.client.service.project_delete(id)
        if not ret.returnCode:
            project = ret.project
        return ret.returnCode, project

    def add_project_members(self, id, rolename, members):
        """Add the members from the array on the role on a given project."""
        ret = self.client.service.add_project_members(id, rolename, members)
        return ret.returnCode, ret.msg

    def del_project_members(self, id, rolename, members):
        """Delete the members from the array on the role on a given project."""
        ret = self.client.service.del_project_members(id, rolename, members)
        return ret.returnCode, ret.msg

    def set_project_members(self, id, rolename, members):
        """Set the members from the array on the role on a given project."""
        ret = self.client.service.set_project_members(id, rolename, members)
        return ret.returnCode, ret.msg

    def get_project_members(self, id, rolename):
        """List the members on a role on an specific project."""
        pMembers = None
        ret = self.client.service.get_project_members(id, rolename)
        if not ret.returnCode:
            pMembers = ret.memberList.item
        return ret.returnCode, pMembers

    def project_get_info(self, id):
        """Retrieve the extra data from a project."""
        projectInfo = None
        ret = self.client.service.get_projectInfo(id)
        if not ret.returnCode:
            projectInfo = ret.projectInfo
        return ret.returnCode, projectInfo

    def project_allow_operators(self, id, value=None):
        """Set the value to the allow operators field."""
        ret = self.client.service.set_allowOperators(id, value)
        return ret.returnCode, ret.msg

    def schema_get(self):
        return NotImplemented
