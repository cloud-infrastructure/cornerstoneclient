# Logger
import logging


class Logger(object):

    logger = None
    ch = None

    @classmethod
    def init_logger(cls):
        cls.logger = logging.getLogger('debugcli')
        # create console handler and set level to debug
        cls.ch = logging.StreamHandler()
        # add ch to logger
        cls.logger.addHandler(cls.ch)

    @classmethod
    def set_debug(cls):
        cls.logger.setLevel(logging.DEBUG)
        cls.ch.setLevel(logging.DEBUG)

    @classmethod
    def get_logger(cls):
        if not cls.logger:
            cls.init_logger()
        return cls.logger


def get_logger():
    return Logger.get_logger()
