# Exception definitions


class CommandError(Exception):
    pass


class AuthenticationError(Exception):
    pass
