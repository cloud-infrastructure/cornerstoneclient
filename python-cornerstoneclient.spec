%global     pypi_name   cornerstoneclient
%global     desc        Client library and command line utility for interacting\
with API server for FIM integration.

Name:       python-cornerstoneclient
Version:    1.6.1
Release:    1%{?dist}
Summary:    Python command line interface for FIM integration API server

License:    ASL 2.0
BuildArch:  noarch
URL:        https://gitlab.cern.ch/cloud-infrastructure/cornerstoneclient
Source0:    %{name}-%{version}.tar.gz

# required for py3_build macro
BuildRequires:  python3-devel

Requires:   python3-prettytable
Requires:   python3-setuptools
Requires:   python3-zeep
Requires:   python3-requests
Requires:   python3-requests-kerberos
Requires:   python3-requests-oauthlib

BuildRequires:  python3-pbr >= 1.8
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

%{?python_enable_dependency_generator}

%description
%{desc}

%package -n python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

%description -n python3-%{pypi_name}
%{desc}

%package doc
Summary:    Documentation for Cornerstone API Client

BuildRequires: python3-sphinx

%description doc
Documentation for the client library for interacting with API server for
FIM integration

%prep
%autosetup -p1 -n %{name}-%{version}

%build
%{py3_build}

# generate html docs 
sphinx-build doc/source html
# remove the sphinx-build leftovers
rm -rf html/.{doctrees,buildinfo}

%install
%py3_install

%files -n python3-%{pypi_name}
%attr(0755, root, root) %{_bindir}/cornerstone
%{python3_sitelib}/%{pypi_name}
%attr(0755, root, root) %{python3_sitelib}/%{pypi_name}/shell.py
%{python3_sitelib}/%{pypi_name}-*-py*.egg-info/

%files doc
%doc html

%changelog
* Fri Feb 07 2025 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6.1-1
- Allow to not use enabled field in project creation
- Adapt block comment error in flake8
- Close vulnerability report and enable generation in tox for man documentation

* Mon Jan 13 2025 Jose Castro Leon <jose.castro.leon@cern.ch> 1.6.0-1
- Add oidc into clients and refactor authentication a bit

* Wed Jan 10 2024 Ulrich Schwickerath <schwicke@cern.ch> 1.5.4-2
- python3 bug fix for iter

* Thu Jul 06 2023 Domingo Rivera Barros <driverab@cern.ch> 1.5.4-1
- Adapt tox.ini
- Build for 9

* Tue Apr 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.5.3-4
- Rebuild for alma8 and rhel8

* Tue Apr 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.5.3-3
- Rebuild for centos8 stream

* Wed Jun 17 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.5.3-2
- Fix issues with kerberos on rest interface

* Tue May 05 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.5.3-1
- Rebuild for centos8

* Mon Nov 18 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 1.5.3-1.el7
- Fix schemas to match requirements from new resources

* Mon Oct 22 2018 Jose Castro Leon <jose.castro.leon@cern.ch> 1.5-2.el7
- Add tests, fix lint issues and include new RPM building procedure

* Mon Oct 23 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.5-1.el7
- Add supporters management in the client

* Tue Oct 17 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.5-0.el7
- Replace suds by zeep library

* Mon Sep 25 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.4-3.el7
- Remove allow accounting as it has been replaced by a different method

* Fri Aug 11 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.4-2.el7
- Remove create security groups call as it is not used

* Mon Jul 31 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.4-1.el7
- Add method for list all services available

* Thu Jul 06 2017 Jose Castro Leon <jose.castro.leon@cern.ch> 1.3-1.el7
- OS-4782 Remove REQUEST_CA_BUNDLE from client

* Mon Apr 25 2016 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.2-1.el7
- OS-2817 Remove pip-requires

* Wed Oct 07 2015 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.1-0.el7
- [os-1593] Add REST API to cornerstone
- [os-1678] Add logging to cornerstoneclient
- [os-1795] Complete Services API
- [os-1796] Add REST Properties call

* Fri Jun 05 2015 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.0-3.el7
- [os-1531] Add allow-service command

* Mon Dec 01 2014 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.0-2.el7
- First CentOS release

* Thu Jun 26 2014 Marcos Fermin Lobo <marcos.fermin.lobo@cern.ch> 1.0-1.slc6
- Documentation updated
- Added new feature domain-list

* Wed Mar 12 2014 Jose Castro Leon <jose.castro.leon@cern.ch> 0.9-1.slc6
- CRUD on role assignment for projects
- Improved lifecycle management for projects
- remove unused functionality

* Fri Feb 21 2014 Jose Castro Leon <jose.castro.leon@cern.ch> 0.5-1.slc6
- Allow accounting and operators actions

* Thu Oct 24 2013 Jose Castro Leon <jose.castro.leon@cern.ch> 0.4-1.ai6
- Set enabled by default on project update

* Fri May 17 2013 Jose Castro Leon <jose.castro.leon@cern.ch> 0.3-1.ai6
- Requires python-httplib2
- Added ownership management

* Fri Mar 15 2013 Jose Castro Leon <jose.castro.leon@cern.ch> 0.2-1.ai6
- Minor fix exception 

* Thu Mar 07 2013 Jose Castro Leon <jose.castro.leon@cern.ch> 0.1-1.ai6
- Initial version of the tool

