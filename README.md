cornerstoneclient
=================

![Build status](https://gitlab.cern.ch/cloud-infrastructure/cornerstoneclient/badges/master/build.svg)
![coverage](https://gitlab.cern.ch/cloud-infrastructure/cornerstoneclient/badges/master/coverage.svg?job=coverage)

cornerstoneclient is a python CLI to test the integration between Cloud and CERN.